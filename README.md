# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

This is a micro CRM web api using W3-CSS,AngularJs,Php,MySQL.


### How do I get set up? ###

** Pre Installation this you need a web server that supports PHP eg : Apache and MySQL database.**

### Guidelines to Run the code ###

 1. Run inputForm.html
  
![inputForm.html.png](https://bitbucket.org/repo/rLnXAb/images/4000093315-inputForm.html.png)

![input.png](https://bitbucket.org/repo/rLnXAb/images/3529895113-input.png)

![success.png](https://bitbucket.org/repo/rLnXAb/images/2286170247-success.png)


![db.png](https://bitbucket.org/repo/rLnXAb/images/3576920904-db.png)

![Screenshot from 2016-09-04 22:59:26.png](https://bitbucket.org/repo/rLnXAb/images/2132521825-Screenshot%20from%202016-09-04%2022:59:26.png)

![Screenshot from 2016-09-04 23:02:05.png](https://bitbucket.org/repo/rLnXAb/images/52147046-Screenshot%20from%202016-09-04%2023:02:05.png)

![Screenshot from 2016-09-04 23:00:14.png](https://bitbucket.org/repo/rLnXAb/images/430617285-Screenshot%20from%202016-09-04%2023:00:14.png)

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact